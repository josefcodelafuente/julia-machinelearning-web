# Install required packages
using Pkg

Pkg.add("CSV")
Pkg.add("DataFrames")
Pkg.add("Plots")
Pkg.add("ScikitLearn")
Pkg.add("DecisionTree")
Pkg.add("JLD2")

# Load required modules
using CSV, DataFrames, Plots, ScikitLearn, DecisionTree, JLD2

# Load training dataset and show summary info
train_df = CSV.read("train.csv",DataFrame)
describe(train_df)

# Process columns with missing values

# Remove rows with missing Embarked because it's just two rows
dropmissing!(train_df,"Embarked")
# Fill missing Age with median age which is 28
train_df.Age = replace(train_df.Age,missing=>28)
# 'Cabin' column has too many missing values (more than 50%), 
# so perhaps better to remove it at all
train_df = select(train_df,Not(["Cabin"]))
    

# Process categorial columns

# Group by Embarked columns to see which categories it has
# and number of rows in each category
embarked_group = groupby(train_df,"Embarked")
combine(embarked_group, nrow=>"Count")
# Encode and convert possible categories to numbers
train_df.Embarked = Int64.(replace(train_df.Embarked, "S" => 1, "C" => 2, "Q" => 3))
# The same with Sex column
combine(groupby(train_df,"Sex"), nrow=>"Count")
train_df.Sex = Int64.(replace(train_df.Sex, "female" => 1, "male" => 2))
# Lets see the same for Ticket column
combine(groupby(train_df,"Ticket"), nrow=>"Count")
# As it has too much categories, perhaps it's not a good idea
# to think that they can be helpful to classify the data without,
# additional preprocessing. So, just remove it
train_df = select(train_df,Not(["Ticket"]))
# The 'Name' and 'PassengerId' columns also not a good options to classify data,
# so remove them as well
train_df = select(train_df,Not(["Name", "PassengerId"]))
# Look at train data summary again to ensure that
# it's ready for training (no missing values, all columns are numeric)
describe(train_df)


# Let's draw some plots that can show some patterns in data related
# to passengers survival which can help us later when verify how the machine learning works

# Group dataset by "Survived" column
survived = combine(groupby(train_df,"Survived"), nrow => "Count")
# Display the data on bar chart
plot(survived.Survived, survived.Count, title="Survived Passengers", 
    label=nothing, seriestype="bar", texts=survived.Count)
# Modify X axis to display text labels instead of numbers
xticks!([0:1:1;],["Not Survived","Survived"])

# Group dataset by Sex column and show only rows where Survived=1
survived_by_sex = combine(groupby(train_df[train_df.Survived .== 1,:],"Sex"), nrow => "Count")
# Display the data on bar chart 
plot(survived_by_sex.Sex, survived_by_sex.Count, title="Survived Passengers by Sex", 
    label=nothing, seriestype="bar", texts=survived_by_sex.Count)
# Modify X axis to display text labels instead of numbers
xticks!([1:1:2;],["Female","Male"])

# Group dataset by PClass column and show only rows where Survived=0
death_by_pclass = combine(groupby(train_df[train_df.Survived .== 0,:],"Pclass"), nrow => "Count")
# Display the data on bar chart 
plot(death_by_pclass.Pclass, death_by_pclass.Count, title="Dead Passengers by Ticket class", label=nothing, seriestype="bar", texts=death_by_pclass.Count)
# Modify X axis to display text labels instead of numbers
xticks!([1:1:3;],["First","Second","Third"])

###########################################
# How to Train Our Machine Learning Model #
###########################################

using DecisionTree, SciKitLearn.CrossValidation

# Extract features matrix and labels vector

# Put "Survived" column to labels vector
y = train_df[:,"Survived"]
# Put all other columns to features matrix (important to convert to "Matrix" data type)
X = Matrix(train_df[:,Not(["Survived"])])

# Create Random Forest Classifier with 100 trees
model = RandomForestClassifier(n_trees=100)

# Train the model, using features matrix and labels vector
fit!(model,X,y)

# Evaluate the accuracy of predictions using Cross Validation
accuracy = minimum(cross_val_score(model, X, y, cv=5))
 
#########################################
# How to Make Predictions               #
#########################################

# Load testing dataset and show summary info
test_df = CSV.read("test.csv", DataFrame)
describe(test_df)
# Save PassengerId field to separate variable
PassengerId = test_df[:,"PassengerId"]

# Repeat the same transformations as we did for training dataset
test_df = select(test_df,Not(["PassengerId","Name","Ticket","Cabin"]))
test_df.Age = replace(test_df.Age,missing=>28)
test_df.Embarked = replace(test_df.Embarked,"S" => 1, "C" => 2, "Q" => 3)
test_df.Embarked = convert.(Int64,test_df.Embarked)
test_df.Sex = replace(test_df.Sex,"female" => 1,"male" => 2)
test_df.Sex = convert.(Int64,test_df.Sex)

# In addition, replace missing value in 'Fare' field with median
test_df.Fare = replace(test_df.Fare,missing=>14.4542)

# Ensure that all data transofrmations applied correctly
describe(test_df)

# Make a prediction for testing dataframe using the trained model
Survived = predict(model, Matrix(test_df)) 


######################################################
# The competition requires a CSV file with two       #
# columns: "PassengerId" and "Survived". You already # 
# have all this data. Let's create the data frame    #
# with these two columns and save it to a CSV file.  #
######################################################
# Create a dataset in a format, required for Kaggle submission (with 'PassengerId' 
# and 'Survived' columns) and save it to CS
submit_df = DataFrame(PassengerId=PassengerId,Survived=Survived)
CSV.write("submission.csv",submit_df)

# submit_df

# Serialize and save the model to a file for deployment in production
save_object("titanic.jld2",model)