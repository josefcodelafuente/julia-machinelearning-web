// Variables
const formulario = document.querySelector('#formulario');

// Funciones
async function validar (evento) {
    // Evitar que se envie el formulario
    evento.preventDefault();

    const data = JSON.stringify({
        "pclass":parseInt(document.getElementById("pclass").value),
        "sex":parseInt(document.getElementById("sex").value),
        "age":parseFloat(document.getElementById("age").value),
        "sibsp":parseInt(document.getElementById("sibsp").value),
        "parch":parseInt(document.getElementById("parch").value),
        "fare":parseFloat(document.getElementById("fare").value),
        "embarked":parseInt(document.getElementById("embarked").value),
    })
    console.log(data);
    const response = await fetch("http://localhost:8090", {
        headers: {
            'Content-type': 'application/json'
          },
        method:"POST",
        body: data
    });
    const survivedCode =  parseInt(await response.text());
    console.log(survivedCode);
    document.getElementById("survived").innerHTML = survivedCode ? "YES" : "NO"
}

// Eventos
formulario.addEventListener('submit', validar);
