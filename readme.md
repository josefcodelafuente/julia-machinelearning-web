# Machine learning with Julia – How to Build and Deploy a Trained AI Model as a Web Service

Fuente: https://www.freecodecamp.org/news/machine-learning-using-julia/

This article introduces the Julia language and its ecosystem. You'll learn how to use it to solve a Titanic machine learning competition and submit it to the Kaggle.

You'll also learn how to deploy your machine learning model to production as a web service and create a web interface to send prediction requests to this service from a web browser.

By the end of the article, you will create a simple AI-powered web application that you can use as a template for creating more complex Julia ML solutions.

## How to Install Julia and Jupyter Notebook Support